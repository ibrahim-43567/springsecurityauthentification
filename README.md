

// rappel : pour quelques elements 

// h2 : application basée sur JSP 
// spring securiyt : de base authentification par les sessions
// pour eviter les attaques CSRF protection

http://localhost:8080/h2-console/ url pour accéder à la base h2

H2 console utilise les frames : 
les frames possèdent des failles de securité, spring security detecte les pages qui contiennent les frames il les désactive

spring security a activé une parade contre les frames
on va le desactiver contre h2 

Ne jamais exposer les mots de passes dans la sérialisation

Note : on a utilisé le même mot de passe mais les hash sont différents 

Bcrypt est plus fort que MD5 : algorithme non symétrique
avec le hash on ne peut pas obtenir le mot de passe original

pour pouvoir récupérer le mdp il faut des algorithmes puissants 

Remarque : les hash sont différents mais meme mdp, 
Bcrypt : quand on crypte la même chaine , la date système est utilisé
donc des valeurs identiques 