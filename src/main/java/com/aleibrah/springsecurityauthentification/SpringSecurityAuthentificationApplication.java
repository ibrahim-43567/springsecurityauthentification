package com.aleibrah.springsecurityauthentification;

import com.aleibrah.springsecurityauthentification.entities.AppRole;
import com.aleibrah.springsecurityauthentification.entities.AppUser;
import com.aleibrah.springsecurityauthentification.service.AccountService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class SpringSecurityAuthentificationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityAuthentificationApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        // encodeur pour nos mot de passe
        return new BCryptPasswordEncoder();
    }

    @Bean
    CommandLineRunner start(AccountService accountService){
        return args->{
            accountService.addAppRole(new AppRole(null,"USER"));
            accountService.addAppRole(new AppRole(null,"ADMIN"));
            accountService.addAppRole(new AppRole(null,"CUSTOMER_MANAGER"));
            accountService.addAppRole(new AppRole(null,"PRODUCT_MANAGER"));
            accountService.addAppRole(new AppRole(null,"BILLS_MANAGER"));

            accountService.addNewUser(new AppUser(null,"1234","user1",new ArrayList<>()));
            accountService.addNewUser(new AppUser(null,"1234","admin",new ArrayList<>()));
            accountService.addNewUser(new AppUser(null,"1234","user2",new ArrayList<>()));
            accountService.addNewUser(new AppUser(null,"1234","user3",new ArrayList<>()));
            accountService.addNewUser(new AppUser(null,"1234","user4",new ArrayList<>()));

            accountService.addRoleToUser("user1","USER");
            accountService.addRoleToUser("admin","USER");
            accountService.addRoleToUser("admin","ADMIN");
            accountService.addRoleToUser("user2","USER");
            accountService.addRoleToUser("user2","CUSTOMER_MANAGER");
            accountService.addRoleToUser("user3","USER");
            accountService.addRoleToUser("user3","PRODUCT_MANAGER");
            accountService.addRoleToUser("user4","PRODUCT_MANAGER");





        };
    }
}
