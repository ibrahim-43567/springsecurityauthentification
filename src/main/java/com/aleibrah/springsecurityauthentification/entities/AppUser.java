package com.aleibrah.springsecurityauthentification.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppUser {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @JsonProperty(access= JsonProperty.Access.WRITE_ONLY)
    private String password;

    private String username;

    @ManyToMany(fetch= FetchType.EAGER)
    private Collection<AppRole> roles;

}
