package com.aleibrah.springsecurityauthentification.service;

import com.aleibrah.springsecurityauthentification.entities.AppRole;
import com.aleibrah.springsecurityauthentification.entities.AppUser;

import java.util.List;

public interface AccountService {

    AppUser addNewUser(AppUser appUser);

    AppRole addAppRole(AppRole appRole);

    void addRoleToUser(String username,String roleName);

    AppUser loadUserByUsername(String username);

    List<AppUser> listUsers();
}