package com.aleibrah.springsecurityauthentification.service;

import com.aleibrah.springsecurityauthentification.entities.AppRole;
import com.aleibrah.springsecurityauthentification.entities.AppUser;
import com.aleibrah.springsecurityauthentification.repository.AppRoleRepository;
import com.aleibrah.springsecurityauthentification.repository.AppUserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;



@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    // note le mieux c'est de na pas utiliser
    private AppUserRepository appUserRepository;

    private AppRoleRepository appRoleRepository;

    private PasswordEncoder passwordEncoder;

    // mieux que autowired
    public AccountServiceImpl(AppUserRepository appUserRepository, AppRoleRepository appRoleRepository, PasswordEncoder passwordEncoder) {
        this.appUserRepository = appUserRepository;
        this.appRoleRepository = appRoleRepository;
        this.passwordEncoder=passwordEncoder;
    }


    @Override
    public AppUser addNewUser(AppUser appUser) {
        String pwd="1234";
        appUser.setPassword(passwordEncoder.encode(pwd));

        return appUserRepository.save(appUser);
    }

    @Override
    public AppRole addAppRole(AppRole appRole) {
        return appRoleRepository.save(appRole);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        AppUser appuser=appUserRepository.findByUsername(username);
        AppRole appRole=appRoleRepository.findByRoleName(roleName);
        appuser.getRoles().add(appRole);

    }

    @Override
    public AppUser loadUserByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }

    @Override
    public List<AppUser> listUsers() {
        return appUserRepository.findAll();
    }
}
